<?php

namespace App\Controller;

use App\Form\Type\PersonneType;
use App\Entity\Personne;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;


class FormController extends AbstractController
{
	/**
	* @Route("/form", name="form_personne")
	*/
    public function new(Request $request)
    {
        // creates a task object and initializes some data for this example
        $personne = new Personne();
        $personne->setNom('Entrez un NOM');
        $personne->setDatedenaissance(\DateTime::createFromFormat("Y/m/d","1996/01/01"));

        $form = $this->get('form.factory')
        	->createNamed('create_personne',PersonneType::class, $personne);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
        	$personne = $form->getData();
        	$now = new \DateTime();
        	$age = $now->diff($personne->getDatedenaissance())->y;
			$personne->setAge($age);
			$telephone = $personne->getTelephone();
			if(preg_match('/^((\+)33|0)[1-9](\d{2}){4}$/', $telephone))
			{
        		$entityManager = $this->getDoctrine()->getManager();
        		$entityManager->persist($personne);
        		$entityManager->flush();
			}
        	else{return $this->render('personne/erreurtelephone.html.twig', [
        	'form' => $form->createView(),
        ]);}


        	return $this->redirectToRoute('personne_show', ['id' => $personne->getIdpersonne()]);
        }

        return $this->render('personne/new.html.twig', [
        	'form' => $form->createView(),
        ]);
    }


	/**
	* @Route("/form/{id}", name="form_edit")
	*/
	public function edit($id, Request $request)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$personne = $entityManager->getRepository(Personne::class)->findByIdpersonne($id);
 		
		if (!$personne) {
			throw $this->createNotFountException(
				'Pas de personne trouvée pour id: ' .$id);
		}

		$form = $this->get('form.factory')
		->createNamed('edit_personne',PersonneType::class, $personne[0]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
        	$personne = $form->getData();
        	$now = new \DateTime();
        	$age = $now->diff($personne->getDatedenaissance())->y;
			$personne->setAge($age);
			$telephone = $personne->getTelephone();
			if(preg_match('/^((\+)33|0)[1-9](\d{2}){4}$/', $telephone))
			{
					$entityManager->persist($personne);
        			$entityManager->flush();			
			}
        	else{return $this->render('personne/erreurtelephone.html.twig', [
        	'form' => $form->createView(),
        ]);}

        	return $this->redirectToRoute('personne_show', ['id' => $id]);
        }
        return $this->render('personne/new.html.twig', [
        	'form' => $form->createView(),
        ]);
	}

	/**
	* @Route("/delete/{id}", name="delete_personne")
	*/
	public function delete($id)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$personne = $entityManager->getRepository(Personne::class)->findByIdpersonne($id);
 		
		if (!$personne) {
			throw $this->createNotFountException(
				'Pas de personne trouvée pour id: ' .$id);
		}

		$entityManager->remove($personne[0]);
		$entityManager->flush();

		return $this->redirectToRoute('personne_show_all');
	}

	/**
	* @Route("/search", name="search_name")
	*/
	public function search(){
		$repository = $this->getDoctrine()
								->getManager()
								->getRepository(Personne::Class);

		if (!empty($_POST['nomPersonne']))
		{
			$nom = htmlspecialchars($_POST['nomPersonne']);
			
			$personnes = $repository->findByNom($nom);

			if (!$personnes) {
				throw $this->createNotFountException(
					'Pas de personne trouvée pour nom: ' .$nom);
			}

			return $this->render('personne/liste.html.twig', [
   			'personnes' => $personnes,
   			'infos' => '',
   			]);
		}
		else{
			$listePersonnes = $repository->findAll();
			return $this->render('personne/liste.html.twig', [
   			'personnes' => $listePersonnes,
   			'infos' => 'Aucune personne trouvée avec ce nom',
   		]);
		}

	}

}
?>