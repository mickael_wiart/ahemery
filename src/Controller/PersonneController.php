<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;


class PersonneController extends AbstractController
{
	

	/**
	* @Route("/all", name="personne_show_all")
	*/
	public function showAll(){
		$repository = $this->getDoctrine()->getRepository(Personne::class);
		$listePersonnes = $repository->findAll();
		return $this->render('personne/liste.html.twig', [
   			'personnes' => $listePersonnes,
   			'infos' => '',
   		]);
	}

    /**
 	* @Route("/personne/{id}", name="personne_show")
 	*/
    public function show($id){

		$repository = $this->getDoctrine()->getRepository(Personne::class);
		$personne = $repository->findByIdpersonne($id);

    	if (!$personne) {
        	throw $this->createNotFoundException(
            	'No product found for id '.$id
       		);
   		}

   		return $this->render('personne/liste.html.twig', [
   			'personnes' => $personne,
   			'infos' => '',
   		]);
    	

	}

}
?>