<?php

namespace App\Form\Type;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

//Class type d'un formulaire concernant une Personne
class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
            	'required' => false,
            ])
            ->add('prenom', TextType::Class, [
            	'required' => false,
            ])
            ->add('datedenaissance', DateType::class, array(
                 'widget' => 'choice',
                 'years' => range(date('1920'), date('2020')),
                 'months' => range(date('1'), 12),
                 'days' => range(date('1'), 31),
               ))
            ->add('telephone', TextType::Class, [
            	'required' => false,
            ])            
            ->add('email', EmailType::Class, [
            	'required' => false,
            	'constraints' => [
            		new Assert\Email([
            			'message' => 'Cette email ne respecte pas le bon format']),
            	],
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configurationOptions(OptionsResolver $resolver)
    {
    	$resolver->setDefaults([
    		'data_class' => Personne::class,
    	]);
    }
}

?>